#!/bin/bash

#Install Docker
sudo apt-get update -y
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y  \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get -y update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose




#Cloning projects 

eval `ssh-agen`
chmod 400 .ssh/sabbar
ssh-add .ssh/sabbar
git clone git@bitbucket.org:sabbar-engineers/sabbar-gigs-backoffice.git
git clone git@bitbucket.org:sabbar-engineers/sabbar-gigs-backend.git

#Cloning dockerization
git clone https://gitlab.com/mohamed.elnmr/sabbar.git
#CERTBOT Configuration
git clone https://github.com/certbot/certbot
cd certbot/
./certbot-auto certonly --manual -d *.staging.giglaya.com

mkdir ssl
cp -L -H /etc/letsencrypt/live/staging.giglaya.com/* ssl/


