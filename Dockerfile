FROM nginx:1.9

COPY default.conf /etc/nginx/conf.d/default.conf
COPY ./ssl/api.giglaya.com/ /etc/letsencrypt/api.giglaya.com
COPY ./ssl/bo.giglaya.com/  /etc/letsencrypt/bo.giglaya.com


CMD ["nginx","-g","daemon off;"]
