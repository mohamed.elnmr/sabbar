db.getSiblingDB('sabbar').createUser({
    user: 'sabbar_admin',
    pwd: 'd^h7#ba*1m',
    roles: [{ role: 'userAdmin', db: 'sabbar' }, { role: 'dbOwner', db: 'sabbar' }, { role: 'dbOwner', db: 'test' }]
})

db.getSiblingDB('admin').createUser({ user: 'sabbar_backup', pwd: 'fh4F9#1mZ8', roles: [{ role: 'backup', db: 'admin' }, { role: 'restore', db: 'admin' }] })
db.getSiblingDB('admin').createUser({ user: 'mongo_admin', pwd: 'a39d(nd*s8', roles: [{ role: 'userAdminAnyDatabase', db: 'admin' }, { role: 'clusterAdmin', db: 'admin' }] })
db.getSiblingDB('admin').createUser({ user: 'mongo_exporter', pwd: 'dj#nL8AvQ}', roles: [{ role: 'clusterMonitor', db: 'admin' }, { role: 'read', db: 'local' }] })