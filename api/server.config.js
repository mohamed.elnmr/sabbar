module.exports = {
    apps: [{
        name: 'sabbar',
        script: 'dist/main.js',
        env: {
            NODE_ENV: 'development',
            PORT: '5555'
        }
    }]
};